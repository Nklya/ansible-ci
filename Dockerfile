FROM debian:buster-slim

LABEL name="ansible-ci"

ARG ANSIBLE_VERSION=2.9.2
ARG DOCKERVERSION=19.03.5

# Install ansible/python3 & test kitchen
RUN DEBIAN_FRONTEND=noninteractive \
  apt-get update -qy \
  && apt-get install --no-install-recommends -qy \
  python3-minimal \
  python3-pip \
  python3-dev \
  python3-setuptools \
  python3-wheel \
  build-essential \
  curl git openssh-client \
  ruby ruby-dev \
  && pip3 install --upgrade cffi \
  && pip3 install ansible==${ANSIBLE_VERSION} \
  && pip3 install molecule ansible-lint yamllint bump2version \
  && gem install -q --no-rdoc --no-ri --no-format-executable --no-user-install \
  test-kitchen:1.24.0 kitchen-ansible kitchen-docker kitchen-inspec inspec \
  && apt-get remove -f -y --purge --auto-remove build-essential python3-pip python3-dev ruby-dev \
  && apt-get clean \
  && apt-get autoremove -y \
  && rm -rf /var/lib/apt/lists/* /tmp/* /root/.cache

# Add Docker client
RUN curl -fsSLO https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKERVERSION}.tgz \
  && mv docker-${DOCKERVERSION}.tgz docker.tgz \
  && tar xzvf docker.tgz \
  && mv docker/docker /usr/local/bin \
  && rm -r docker docker.tgz

# Copy liter rules
COPY ansible-lint.yml yamllint.yml /opt/

# Add non-root user 'ansible'
RUN groupadd -r ansible && useradd -r -s /bin/false -m -g ansible ansible
USER ansible

WORKDIR /
CMD [ "ansible", "--version" ]
